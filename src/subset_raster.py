# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 14:14:32 2018

@author: plousser
"""



import os, os.path, optparse, sys


def subset_raster(input_file,output_file, xmin=None,xmax=None,ymin=None,ymax=None,shapefile=None) :
    
    if shapefile is not None:
        
        os.system(r'gdalwarp -cutline {0} -crop_to_cutline -overwrite -dstnodata -999.0 {1} {2}'.format(shapefile,input_file,output_file))
        
    elif xmin is not None  and xmax is not None and ymin is not None and ymax is not None: 
        #os.command()
        print(xmin)
        print(ymin)
        print(xmax)
        print(ymax)
        print(input_file)
        print(output_file)
        os.system(r'gdalwarp -to SRC_METHOD=NO_GEOTRANSFORM -overwrite -te {0} {1} {2} {3} {4} {5}'.format(xmin,ymin,xmax,ymax,input_file,output_file))
    else : 
        print('You must provide a shapefile or extent coordinates for clipping the ', input_file, 'file')
        

##########################################################################                
            
if __name__ == '__main__':
    
 ##########################################################################                          

    from properties.p import Property
    #import quicklook_raster
    import quicklook
    if os.path.isfile('/projects/subset_raster/conf/configuration.properties'):
        configfile='/projects/subset_raster/conf/configuration.properties'
        prop=Property()
        prop_dict=prop.load_property_files(configfile)
        print(prop_dict)
        print(type(prop_dict['shapefile']))
        if prop_dict['shapefile']:
            subset_raster(prop_dict['input_file'],prop_dict['shapefile']) 
        else :
            subset_raster(prop_dict['input_file'],prop_dict['output_file'],prop_dict['xmin'],prop_dict['xmax'],prop_dict['ymin'],prop_dict['ymax'])
        
        #quicklook_raster.main(prop_dict['output_file'])
        quicklook.quickL(prop_dict['output_file'], prop_dict['output_file'].replace('.tiff','quicklook'), 2, 'sig', 'png') 